﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    GameObject hpGauge;
    int HP = 10;

    void Start()
    {
        hpGauge = GameObject.Find("hpGauge");
    }

   public void DecreaseHp()
    {
        this.hpGauge.GetComponent<Image>().fillAmount -= 0.1f;
        HP -= 1;
        if(HP == 0)
        {
            SceneManager.LoadScene("Gameover");
        }
    }
    
}
